import json
import unittest
from unittest import mock
from flask import request
from app import app
from app.mutations import has_mutation

class ApiTestCase(unittest.TestCase):

    def test_empty_data(self):
        with app.test_client() as client:
            result = client.post('/mutation', json=[])
            self.assertEqual(result.status_code, 400)
            result = client.post('/mutation', json={'dna': []})
            self.assertEqual(result.status_code, 400)

    def test_dna_incomplete(self):
        with app.test_client() as client:
            result = client.post('/mutation', json={'dna': ["AT", "C"]})
            self.assertEqual(result.status_code, 400)

    def test_api_with_mutant(self):
        with app.test_client(self) as client:
            result = client.post('/mutation', json={'dna': ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]})
            self.assertEqual(result.status_code, 200)

    def test_api_no_mutation(self):
        with app.test_client() as client:
            result = client.post('/mutation', json={'dna': ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "CTGTCA", "TCACTG"]})
            self.assertEqual(result.status_code, 403)

    def test_api_with_incorrect_dna(self):
        url = '/mutation'
        with app.test_client() as client:
            result = client.post(url, json={'dna': ["XL/", "NWT", "SLÑ"]})
            self.assertEqual(result.status_code, 400)



