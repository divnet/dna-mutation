import unittest
from app.mutations import has_mutation
from app.settings import DNA_MIN_MUTATIONS

class Has_Mutations(unittest.TestCase):

    def test_is_mutant(self):
        data = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] 
        self.assertEqual(has_mutation(data, 2), True)
        
    def test_horizontal(self):
        data = ["ATCCCC", "CAGTGC", "TTACAT", "AAAAGG", "GCGTCA", "CTACCT"] 
        self.assertEqual(has_mutation(data, 2), True)
        
    def test_horizontal_no_mutation(self):
        data = ["ATCCCT", "CAGTGC", "TTACAT", "ACACGG", "ACGTTA", "CTATCT"] 
        self.assertEqual(has_mutation(data, 1), False)
        
    def test_oblique_no_mutation(self):
        data = ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"] 
        self.assertEqual(has_mutation(data, 2), False)
        
    def test_oblique_inverted(self):
        data = ["ATGCGA", "CAGTGC", "TTCCCT", "AGCCGG", "GCGTCA", "CCACTC"] 
        self.assertEqual(has_mutation(data, 1), True) 
        
    def test_oblique_inverted_no_mutation(self):
        data = ["ATGCGA", "CAGTGC", "TTCACT", "AGCCGG", "GCGTCA", "CCACTT"] 
        self.assertEqual(has_mutation(data, 1), False)
        
    def test_no_mutation(self):
        data = ["ATGCCC", "CAGTGC", "TTACAT", "AGACGG", "GCGTCA", "CTACCT"] 
        self.assertEqual(has_mutation(data, 2), False)