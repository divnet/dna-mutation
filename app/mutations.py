DNA_MUTATION = ('A','T','C','G')
DNA_MUTATION_LENGTH = 4
MUTATION_SEQUENCIES = ['AAAA','TTTT','CCCC','GGGG']

def has_mutation(dna, min_mutations):
    try:
        dna_length = len(dna)
        if dna_length >= DNA_MUTATION_LENGTH and False in [len(r) == dna_length and len(r) >= DNA_MUTATION_LENGTH for r in dna]:
            print("Error: La longitud de las cadenas no coincide")
            
        count_mutations = 0
        for ms in MUTATION_SEQUENCIES:
            count_mutations += is_mutant(dna, ms)
            
        return count_mutations >= min_mutations
        
    except Exception as e:
        raise e

def is_mutant(dna, ms):
    m = 0
    data = {'o': [], 'o_i': []}
    
    for k,val in enumerate(dna):
        # Horizontal
        m += 1 if ms in val else 0
        
        v = ''
        # vertical
        for j in range(0, len(dna)):
            v += dna[j][k]
        m += 1 if ms in v else 0
    
    for k in range(0, len(dna)):
        # Oblique 
        data['o'].append(dna[k][k])
        
        # Oblique inverted
        data['o_i'].append(dna[::-1][k][k])
    
    m += mutation(ms, concat(data['o']))
    m += mutation(ms, concat(data['o_i']))
    
    return m
    
def concat(seq):
    return ''.join(seq)

def mutation(csq, seq):
    return 1 if csq in seq else 0
    
def valid_dna(dna):
    return len([x for x in dna for i in str(x).upper() if i not in DNA_MUTATION]) > 0