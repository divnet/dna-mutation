from flask import Flask
from flask_pymongo import PyMongo
from . import settings

app = Flask(__name__)
app.config["MONGO_URI"] = settings.MONGO_URI
mongo = PyMongo(app)

from . import api
