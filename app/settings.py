from dotenv import load_dotenv
import os

load_dotenv()

MONGO_INITDB_ROOT_USERNAME = os.getenv('MONGO_INITDB_ROOT_USERNAME')
MONGO_INITDB_ROOT_PASSWORD = os.getenv('MONGO_INITDB_ROOT_PASSWORD')
MONGO_INITDB_DATABASE = os.getenv('MONGO_INITDB_DATABASE')

MONGO_URI = os.getenv('MONGO_URI') or f"mongodb://{MONGO_INITDB_ROOT_USERNAME}:{MONGO_INITDB_ROOT_PASSWORD}@db/{MONGO_INITDB_DATABASE}?authSource=admin&retryWrites=true&w=majority"
DNA_MIN_MUTATIONS = 2