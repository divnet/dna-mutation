from flask import Flask
from flask import jsonify
from flask import request, Response
import json
from . import app as app, mongo
from .mutations import valid_dna, has_mutation
from .settings import DNA_MIN_MUTATIONS

@app.route('/', methods=['GET'])
def main():
    return Response('OK', status=200, mimetype='application/json')

# Get stats from mutations
@app.route('/stats', methods=['GET'])
def get_stats():
    count_mutations = mongo.db.adn.count_documents({'is_mutation': True})
    count_no_mutation = mongo.db.adn.count_documents({'is_mutation': False})
    
    ratio = count_mutations/count_no_mutation if count_mutations and count_no_mutation else count_mutations+count_no_mutation
    
    data = {'count_mutations': count_mutations, 'count_no_mutation': count_no_mutation, 'ratio': ratio}
    return Response(json.dumps(data), status=200, mimetype='application/json')

# Register ADN mutations 
@app.route('/mutation', methods=['POST'])
def get_mutation():
    data = request.get_json()
    if not data or (data and not data.get('dna')):
        return Response('Error in data', status=400, mimetype='application/json')
    
    dna = data.get('dna')
    
    for r in dna:
        if len(r) != len(dna):
            return Response('Error in data', status=400, mimetype='application/json')
    
    if valid_dna(dna):
        return Response('Only A, C, T, G chars are allowed', status=400, mimetype='application/json')
    
    find_dna = mongo.db.adn.count_documents({"dna": {"$all": data.get('dna')},'is_mutation': True })
    
    if find_dna:
        return Response('OK', status=200, mimetype='application/json')
    else:
        exists_no_mutant_adn = mongo.db.adn.count_documents({"dna": {"$all": data.get('dna')},'is_mutation': False })
        if exists_no_mutant_adn:
            return Response('Forbidden', status=403, mimetype='application/json')
        
    is_mutant = has_mutation(data.get('dna'), DNA_MIN_MUTATIONS)
    if is_mutant:
        mongo.db.adn.insert_one({'dna': data.get('dna'),'is_mutation': True})
        return Response('OK', status=200, mimetype='application/json')
    
    mongo.db.adn.insert_one({'dna': data.get('dna'), 'is_mutation': False})
    
    return Response('Forbidden', status=403, mimetype='application/json')

    return jsonify(dna)

