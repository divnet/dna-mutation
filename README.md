# dna-mutation
# Descripción
Este un servicio que se encuentra alojado en AWS ECS con un balanceo la carga de acuerdo a la demanda de procesamiento

NOTA: el proposito de docker-compose.yml es solo para ambiente de prueba (local) que no corresponde al ambiente de producción

#### Acceso
- Acceso a API: http://ec2co-ecsel-4mivged57k1s-1564588968.us-east-2.elb.amazonaws.com/

GET /stats
-  http://ec2co-ecsel-4mivged57k1s-1564588968.us-east-2.elb.amazonaws.com/stats

POST /mutation
-  http://ec2co-ecsel-4mivged57k1s-1564588968.us-east-2.elb.amazonaws.com/mutation

### Configuracion
- Las siguientes variables corresponden a .env
```
`FLASK_APP=app` * requerido
`FLASK_ENV` # Opcional
`FLASK_RUN_PORT` # Opcional (default 80)
`MONGO_INITDB_ROOT_USERNAME` * requerido - usuario (se creara dicho usuario en la db)
`MONGO_INITDB_ROOT_PASSWORD` * requerido - contraseña (se creara dicha contraseña para el usuario en la db)
`MONGO_INITDB_DATABASE` * requerido - base de datos (se creara dicha db)
```

## Uso 🚀
* Renombrar el archivo .env.test a .env
* Levantar los servicios necesarios con docker para la API:


```
docker-compose up -d
```

#### Testing (Pruebas unitarias)
```
python3 -m pytest
```

⌨️ [Eduardo Mondragon](https://github.com/n3srcc)