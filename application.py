from app import app
import os

port = os.getenv('FLASK_RUN_PORT') or 80
debug = os.getenv('FLASK_DEBUG') or False

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.getenv('FLASK_RUN_PORT'), debug=debug)