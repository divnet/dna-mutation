FROM ubuntu

RUN apt-get update -y && \
    apt-get install -y python3-pip python-dev

RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/

COPY requirements.txt /usr/src/app/
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /usr/src/app/

ENTRYPOINT ["python3"]

CMD ["application.py"]